# Overview

This project contains a series of Roles that isolate a particular problem, use case or commonly used technique.

## Roles

* [executable-list-testing](executable-list-testing/README.md) - This role is used to demonstrate how Loops, Register (registered) variables and outputs can be used.

## How to run each

Each Role is generated with Molecule and subsequently tested with such. Please install the latest Molecule, then change directory into each of the roles.

```bash
# Run demonstration (can and should be repeated over and over)
molecule converge

# Destroy/cleanup (when done)
molecule destroy

```