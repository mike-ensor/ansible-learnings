# Overview


This role demonstrates how to use `loop` and `register` to obtain status or output from upstream tasks in a role

## Run

```bash

molecule converge

```